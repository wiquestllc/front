<section id="logo-banner">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
	<div id="logo-container">
	  <a id="logo-text" href="<? echo esc_url(home_url()); ?>">Peer M. Amir</a>
	</div>
      </div>
	<div class="col-md-4 col-md-offset-4">
	  <div id="header-address">
	      <span>Contact:</span><br> Latif Khan Free Hospital <br> Opposite Suhag Marriage Hall
	      <br> Railway Bazar G.T. Road Sadhoke<br> Tehsil Kamoke Gujranwala
	      <span style="font-weight: bold;">
	  <br> <span class="glyphicon glyphicon-earphone" aria-hidden="true">  </span><a href="tel: +92-301-6660934">0301-6660934</a>, <a href="tel: +92-333-8211405">0333-8211405</a>
	      </span>
	  </div>
	</div>
    </div>
  </div>
</section>

<section id="nav-holder">  
  <div class="container">
    <div class="row">
      <?php wp_nav_menu( array('menu' => 'Main', 'menu_class' => 'nav nav-pills', 'depth'=> 3, 'container'=> false, 'walker'=> new Bootstrap_Walker_Nav_Menu)); ?>
    </div>
  </div>
</section>

<?php get_header(); ?>

<div class="row">

	<div class="col-md-8">
	<?php if (is_home()): ?>
	<?php echo do_shortcode('[image-carousel]');?>
	<?php endif; ?>
		<?php if(have_posts()) : ?>
		   <?php while(have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<a href="<? the_permalink(); ?>"><?php the_title('<h2>','</h2>'); ?></a>
		 		<?php the_content(); ?>
			</div>
		   <?php endwhile; ?>


		<?php else : ?>

		<div class="alert alert-info">
		  <strong>No content in this loop</strong>
		</div>

		<?php endif; ?>
	</div>

	<div class="col-md-4">

		<?php
		 if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar')) : //  Sidebar name
		?>
		<?php
		     endif;
		?>
	</div>

</div>




<?php get_footer(); ?>